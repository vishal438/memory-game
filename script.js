const gameContainer = document.getElementById("game");
const startBtn = document.getElementById("start");
const restartBtn = document.getElementById("restart");
const moveCount = document.getElementById("move-count");
const bestScoreCard = document.getElementById("best-score-number");
const winMessage = document.getElementById("message");
const instructions = document.getElementById("instructions");
gameContainer.style.display = "none";
restartBtn.style.display = "none";

const GIFS = [
  "./gifs/1.gif",
  "./gifs/2.gif",
  "./gifs/3.gif",
  "./gifs/4.gif",
  "./gifs/5.gif",
  "./gifs/6.gif",
  "./gifs/7.gif",
  "./gifs/8.gif",
  "./gifs/9.gif",
  "./gifs/10.gif",
];
// Function to generate a random number of cards
function RandomCards() {
  const level = [4, 6, 8, 10, 12, 14];
  const randomNumberIndex = Math.floor(Math.random() * 5);
  console.log(randomNumberIndex);
  return level[randomNumberIndex];
}
// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}
let randomNumber = RandomCards();
const shuffledGifs = shuffle(GIFS.slice(0, randomNumber / 2));
const cardPairs = [...shuffledGifs, ...shuffledGifs];
const shuffledCards = shuffle(cardPairs);

// this function loops over the array of Gifs
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function divsInPlaceOfGifs(gifArray) {
  for (let gif of gifArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(gif);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

// TODO: Implement this function!
function handleCardClick(event) {
  // instructions.style.display = "none";
  const clickedCard = event.target;
  clickedCard.classList.add("card-counter");
  const cardCounter = document.querySelectorAll(".card-counter");
  if (cardCounter.length <= 2) {
    const cardClasses = clickedCard.getAttribute("class").split(" ");
    const gifBackground = cardClasses[0];
    clickedCard.style.backgroundImage = `url(${gifBackground})`;
    clickedCard.style.backgroundSize = "cover";
    clickedCard.classList.add("rotate-card");
    if (cardCounter.length === 2) {
      if (cardCounter[0].classList[0] === cardCounter[1].classList[0]) {
        for (let index = 0; index < cardCounter.length; index++) {
          cardCounter[index].removeEventListener("click", handleCardClick);
        }
        cardCounter[1].classList.add("matched-card");
        cardCounter[0].classList.add("matched-card");

        if (
          document.getElementsByClassName("matched-card").length ===
          cardPairs.length
        ) {
          if (localStorage.getItem("best-score") === null) {
            let bestScore = Number(moveCount.textContent) + 1;
            localStorage.setItem("best-score", `${bestScore}`);
            bestScoreCard.textContent = `${bestScore}`;
            winMessage.style.backgroundColor = "white";
            winMessage.textContent = "Played Well";
            restartBtn.style.display = "flex";
          } else {
            let currBestScore = localStorage.getItem("best-score");
            if (Number(moveCount.textContent) + 1 <= Number(currBestScore)) {
              winMessage.style.backgroundColor = "white";
              winMessage.textContent = "Congrats You won!";
              let newBestScore = Number(moveCount.textContent) + 1;
              localStorage.setItem("best-score", `${newBestScore}`);
              bestScoreCard.textContent = newBestScore;
              restartBtn.style.display = "flex";
            } else {
              winMessage.style.backgroundColor = "white";
              winMessage.textContent = "Well Played Better Luck Next Time!";
              localStorage.setItem("best-score", `${currBestScore}`);
              bestScoreCard.textContent = currBestScore;
              restartBtn.style.display = "flex";
            }
          }
        }
        MatchingCards(cardCounter);
      } else {
        setTimeout(() => {
          for (let index = 0; index < cardCounter.length; index++) {
            cardReset(cardCounter[index]);
          }
          closeMismatchedCards(cardCounter);
        }, 1000);
      }
      if (moveCount.textContent === "0") {
        moveCount.textContent = 1;
      } else {
        moveCount.textContent = 1 + Number(moveCount.textContent);
      }
    }
  }
}
// Reset card appearance to original state
function cardReset(card) {
  card.classList.remove("rotate-card");
  card.style.background = "black";
}
// Close matching cards
function MatchingCards(cards) {
  for (let index = 0; index < cards.length; index++) {
    cards[index].classList.remove("card-counter");
  }
}
// Close mismatched cards and revert their appearance
function closeMismatchedCards(cards) {
  for (let index = 0; index < cards.length; index++) {
    cards[index].classList.remove("card-counter");
    cardReset(cards[index]);
  }
}

divsInPlaceOfGifs(shuffledCards);

startBtn.addEventListener("click", () => {
  // Start button click event listener
  instructions.style.display = "none";
  instructions.style.display = "none";
  gameContainer.style.display = "flex";
  if (localStorage.getItem("best-score") === null) {
    bestScoreCard.textContent = 0;
  } else {
    bestScoreCard.textContent = localStorage.getItem("best-score");
  }
  startBtn.style.display = "none";
});

restartBtn.addEventListener("click", () => {
  // Reset game state and shuffle cards
  gameContainer.textContent = "";
  moveCount.textContent = 0;
  winMessage.textContent = "";
  const newRandomNumber = RandomCards();
  const newShuffledGifs = shuffle(GIFS.slice(0, newRandomNumber / 2));
  const newCardPairs = [...newShuffledGifs, ...newShuffledGifs];
  const newShuffledCards = shuffle(newCardPairs);
  shuffledCards.length = 0;
  shuffledCards.push(...newShuffledCards);
  divsInPlaceOfGifs(shuffledCards);

  restartBtn.style.display = "none";
});
// Load event listener to set the best score from local storage
window.addEventListener("load", () => {
  if (localStorage.getItem("best-score") === null) {
    bestScoreCard.textContent = 0;
  } else {
    bestScoreCard.textContent = localStorage.getItem("best-score");
  }
});
